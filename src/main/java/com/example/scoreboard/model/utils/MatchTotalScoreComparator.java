package com.example.scoreboard.model.utils;

import com.example.scoreboard.model.Match;

import java.util.Comparator;

public class MatchTotalScoreComparator implements Comparator<Match> {
    @Override
    public int compare(Match o1, Match o2) {
        int o1TotalScore = o1.getHome().getScore() + o1.getAway().getScore();
        int o2TotalScore = o2.getHome().getScore() + o2.getAway().getScore();

        if (o1TotalScore == o2TotalScore) {
            return o1.getInsertedTime() > o2.getInsertedTime() ? 1 : -1;
        }
        return o1TotalScore < o2TotalScore ? 1 : -1;
    }
}
