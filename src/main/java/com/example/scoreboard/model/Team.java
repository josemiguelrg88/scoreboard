package com.example.scoreboard.model;

import lombok.Data;

@Data
public class Team {

    private final String name;
    private int score;

    public Team(String name) {
        this.name = name;
        this.score = 0;
    }

    public Team(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public void updateScore(int score) {
        this.score = score;
    }

    public String toString() {
        return name + " " + score;
    }
}
