package com.example.scoreboard.model;

import lombok.Getter;

@Getter
public class Match {

    private Team home;
    private Team away;
    private final long insertedTime;

    public Match() {
        insertedTime = System.currentTimeMillis();
    }

    public void setHome(Team home) {
        this.home = home;
    }

    public void setAway(Team away) {
        this.away = away;
    }

    public String printScore() {
        return home.getScore() + " - " + away.getScore();
    }
}
