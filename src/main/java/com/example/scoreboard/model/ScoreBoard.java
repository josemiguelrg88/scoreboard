package com.example.scoreboard.model;

import com.example.scoreboard.model.utils.MatchTotalScoreComparator;
import lombok.Getter;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@Getter
public class ScoreBoard {

    private List<Match> matches;
    private static ScoreBoard scoreBoard;

    private ScoreBoard() {
        this.matches = new LinkedList<>();
    }

    public static ScoreBoard getScoreBoard() {
        if (Objects.isNull(scoreBoard)) {
            scoreBoard = new ScoreBoard();
        }
        return scoreBoard;
    }

    public static void resetScoreBoard() {
        scoreBoard = new ScoreBoard();
    }

    public void addData(String data) throws Exception {
        if (Objects.isNull(data) || data.isEmpty()) {
            wrongDataException();
        }

        String[] teams = data.replaceAll("\\s+", "").split("-");

        if (teams.length != 2) {
            wrongDataException();
        }

        Match match = new Match();
        match.setHome(new Team(teams[0]));
        match.setAway(new Team(teams[1]));

        matches.add(match);
    }

    private void wrongDataException() throws Exception {
        throw new Exception("Wrong data");
    }


    public boolean removeMatch(Match match) {
        return matches.remove(match);
    }

    public void updateScore(Match match, String score) throws Exception {
        for (Match m : matches) {
            if (m.equals(match)) {
                try {
                    int[] scores = Arrays.stream(score.replaceAll("\\s+", "").split("-")).mapToInt(Integer::parseInt).toArray();
                    if (scores.length < 2) {
                        wrongDataException();
                    }
                    m.getHome().updateScore(scores[0]);
                    m.getAway().updateScore(scores[1]);
                } catch (NumberFormatException e) {
                    wrongDataException();
                }
            }
        }
    }

    public void showSummary() {
        List<Match> sortedMatches = new LinkedList<>(matches);
        sortedMatches.sort(new MatchTotalScoreComparator());
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < sortedMatches.size(); i++) {
            Match match = sortedMatches.get(i);
            sb.append(i + 1);
            sb.append(".");
            sb.append("\t");
            sb.append(match.getHome().toString());
            sb.append(" - ");
            sb.append(match.getAway().toString());
            sb.append("\n");
        }
        System.out.println(sb);
    }
}
