package com.example.scoreboard.model;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
public class ScoreBoardTest {


    @After
    public void reset() {
        ScoreBoard.resetScoreBoard();
    }

    /**
     * Start a game requirement.
     */
    @Test
    public void receiveData() throws Exception {
        String data = "Mexico - Canada";
        String teamHome = "Mexico";
        String teamAway = "Canada";

        ScoreBoard.getScoreBoard().addData(data);
        Assert.assertEquals(1,
                ScoreBoard.getScoreBoard().getMatches().stream()
                        .filter(m -> m.getHome().getName().equals(teamHome) && m.getAway().getName().equals(teamAway))
                        .count());
    }

    /**
     * Start a game requirement.
     */
    @Test(expected = Exception.class)
    public void addWrongData() throws Exception {
        String data = "Mexico";

        ScoreBoard.getScoreBoard().addData(data);
    }

    /**
     * Finish game requirement.
     */
    @Test
    public void removeMatch() throws Exception {
        String data = "Mexico - Canada";

        ScoreBoard.getScoreBoard().addData(data);
        ScoreBoard.getScoreBoard().removeMatch(ScoreBoard.getScoreBoard().getMatches().get(0));
        Assert.assertEquals(0, ScoreBoard.getScoreBoard().getMatches().size());
    }

    /**
     * Finish game requirement.
     */
    @Test
    public void removeNonExistenMatch() throws Exception {
        String data = "Mexico - Canada";

        ScoreBoard.getScoreBoard().addData(data);
        Assert.assertFalse(ScoreBoard.getScoreBoard().removeMatch(new Match()));
    }

    /**
     * Update score requirement.
     * Suppose we update score for a concrete game
     */
    @Test
    public void updateMatchScore() throws Exception {
        String data = "Mexico - Canada";
        String score = "2 - 1";

        ScoreBoard.getScoreBoard().addData(data);
        ScoreBoard.getScoreBoard().updateScore(ScoreBoard.getScoreBoard().getMatches().get(0), score);
        Assert.assertEquals(score, ScoreBoard.getScoreBoard().getMatches().get(0).printScore());
        score = "3 - 1";
        ScoreBoard.getScoreBoard().updateScore(ScoreBoard.getScoreBoard().getMatches().get(0), score);
        Assert.assertEquals(score, ScoreBoard.getScoreBoard().getMatches().get(0).printScore());
    }

    /**
     * Update score requirement.
     */
    @Test(expected = Exception.class)
    public void updateMatchScoreWithWrongData() throws Exception {
        String data = "Mexico - Canada";
        String score = "2 - ";

        ScoreBoard.getScoreBoard().addData(data);
        ScoreBoard.getScoreBoard().updateScore(ScoreBoard.getScoreBoard().getMatches().get(0), score);
    }

    /**
     * Summary ordered
     */
    @Test
    public void getSummaryByTotalScore() throws Exception {

        Match m1 = new Match();
        m1.setHome(new Team("Mexico"));
        m1.setAway(new Team("Canada", 5));

        Match m2 = new Match();
        m2.setHome(new Team("Spain", 10));
        m2.setAway(new Team("Brazil", 2));

        Match m3 = new Match();
        m3.setHome(new Team("Germany", 2));
        m3.setAway(new Team("France", 2));

        Match m4 = new Match();
        m4.setHome(new Team("Uruguay", 6));
        m4.setAway(new Team("Italy", 6));

        Match m5 = new Match();
        m5.setHome(new Team("Argentina", 3));
        m5.setAway(new Team("Australia", 1));

        ScoreBoard.getScoreBoard().getMatches().add(m1);
        ScoreBoard.getScoreBoard().getMatches().add(m2);
        ScoreBoard.getScoreBoard().getMatches().add(m3);
        ScoreBoard.getScoreBoard().getMatches().add(m4);
        ScoreBoard.getScoreBoard().getMatches().add(m5);

        ScoreBoard.getScoreBoard().showSummary();
    }

}